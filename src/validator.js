const Joi = require('@hapi/joi');
const constants = require('./com/cationeng/constants');

const { CASE_STATUS } = constants;

/**
 * Method of validation for create case functionality
 *
 * @param body
 * @returns {Promise.<T>|*|Request}
 */
const createCaseValidator = body => {
    const schema = Joi.object().keys({
        title: Joi.string().required(),
        description: Joi.string().allow(''),
        status: Joi.string().valid(Object.keys(CASE_STATUS)).allow('').default(CASE_STATUS.TODO),
    });

    return schema.validate(body);
};

module.exports = {
    createCaseValidator,
};
