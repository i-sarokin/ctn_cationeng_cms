const CASE_STATUS = {
    TODO: 'TODO',
    IN_PROGRESS: 'IN_PROGRESS',
    DONE: 'DONE',
};

module.exports = {
    CASE_STATUS,
};
