const api = require('lambda-api')();

const handleCreateCase = require('./create-case');
const handleUpdateCase = require('./update-case');
const handleGetCase = require('./get-case');
const handleAddCommentCase = require('./add-comment-case');
const handleChangeStatusCase = require('./change-status-case');

api.post('/case', async (req, res) => {
    try {
        const data = await handleCreateCase(req.body);

        res.send(data);
    } catch (error) {
        res.status(error.code).send(error)
    }
});

api.get('/case/:id', async (req, res) => {
    try {
        const data = await handleGetCase({ ...req.body, ...req.pathParameters });

        res.send(data);
    } catch (error) {
        res.status(error.code).send(error)
    }
});

api.put('/case/:id', async (req, res) => {
    try {
        const data = await handleUpdateCase({ ...req.body, ...req.pathParameters });

        res.send(data);
    } catch (error) {
        res.status(error.code).send(error)
    }
});

api.put('/case/:id/status', async (req, res) => {
    try {
        const data = await handleChangeStatusCase({ ...req.body, ...req.pathParameters });

        res.send(data);
    } catch (error) {
        res.status(error.code).send(error)
    }
});

api.post('/case/:id/comment', async (req, res) => {
    try {
        const data = await handleAddCommentCase({ ...req.body, ...req.pathParameters });

        res.send(data);
    } catch (error) {
        res.status(error.code).send(error)
    }
});

module.exports.handler = (event, context, callback) => {
    console.info(JSON.stringify({ event, context, callback }, null, 4));
    api.run(event, context, callback);
};
