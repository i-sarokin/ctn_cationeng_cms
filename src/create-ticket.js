const { createCaseValidator } = require('./validator');

const main = async body => {
    console.info('!!!!', body);
    return createCaseValidator(body)
        .then(newBody => ({
            action: 'create-case',
            message: 'Will be implemented',
            body: newBody,
        }));
};

module.exports = main;
