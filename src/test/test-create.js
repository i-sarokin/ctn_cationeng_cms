const chai = require('chai');
const handler = require('../create-ticket');


describe('LAMBDA: ctn_cationeng_cms \n  ACTION: create-case', function () {
    it('Create a new case', function (done) {
        this.timeout(10000);

        const cb = (err, result) => {
            console.log(result);
            done();
        };

        const context = {
            succeed: a => {
                chai.expect('Will be implemented').to.equal(a.message);
                console.log('succeed', JSON.stringify(a, null, 4));
                done();
            },
            fail: b => {
                console.error('fail', b);
                done(new Error(b));
            },
        };

        const query = {
            title: 'First case',
            description: 'First case description',
            status: 'TODO',
        };

        handler(query);
    });
});
