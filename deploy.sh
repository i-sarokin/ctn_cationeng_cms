#!/usr/bin/env bash

PROFILE=cation-edtech
SOURCE_BUCKET=ctn-git2codepipeline-544146838547.cationeng.com

YELLOW='\033[0;33m';
DEFAULT='\033[0;0m';
BLUE='\033[0;34m';

# Delete old code
echo -e "${BLUE}Delete old temp data${DEFAULT}";
rm -rf .build ctn_cationeng_cms_build.zip;
mkdir -p .build

# Copy code for production build
echo -e "${BLUE}Install production modules${DEFAULT}";
cp package.json .build/package.json
cd .build
npm install --production

cd ../

echo -e "${BLUE}Copy code for build in production mode${DEFAULT}";
rsync -av src/ .build/;

# Create a zip archive with layer
echo -e "${BLUE}Make: ${YELLOW}ctn_cationeng_cms_build.zip ${BLUE}build${DEFAULT}";
cd .build/
zip -r ../ctn_cationeng_cms_build.zip ./
cd ../

# Upload function to s3
echo -e "${BLUE}Upload: ${YELLOW}ctn_cationeng_cms ${BLUE}build to s3${DEFAULT}";
aws s3 cp ctn_cationeng_cms_build.zip s3://${SOURCE_BUCKET}/ctn_cationeng_cms_build.zip --profile ${PROFILE};

# Deploy CloudFormation stack --profile cation-edtech
echo -e "${BLUE}Deploy CloudFormation stack${DEFAULT}";
aws cloudformation deploy \
    --template-file cloudformation.yaml \
    --stack-name ctn-cationeng-cms \
    --capabilities CAPABILITY_NAMED_IAM \
    --profile ${PROFILE};

# Delete temp folder
echo -e "${BLUE}Delete temp data${DEFAULT}";
rm -rf .build
